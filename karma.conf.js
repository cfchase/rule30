module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'public/js/lib/lodash/lodash.js',
            'public/js/lib/jquery/jquery-2.1.1.js',
            'public/js/lib/bootstrap/bootstrap.js',
            'public/js/lib/angular/angular.js',
            'public/js/lib/angular/angular-animate.js',
            'public/js/lib/angular/angular-cookies.js',
            'public/js/lib/angular/angular-route.js',
            'public/js/lib/angular/angular-sanitize.js',
            'public/js/lib/angular/angular-mocks.js',
            'public/js/app.js',
            'public/js/services.js',
            'public/js/controllers.js',
            'public/js/directives.js',
            'test/helpers/*.js',
            'test/spec/*.js'
        ],
        exclude: [],
        port: 8080,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],
        singleRun: false
    });
};
