describe('Service: RuleMatrix', function () {
    'use strict';

    var RuleMatrix;

    beforeEach(module('RuleVisualizer'));

    beforeEach(inject(function (_RuleMatrix_) {
        RuleMatrix = _RuleMatrix_;
    }));

    describe('Rule 0', function () {
        var ruleMatrix;

        beforeEach(function () {
            ruleMatrix = new RuleMatrix(0);
        });

        it('should create the correct RuleMatrix for 0', function () {
            expect(ruleMatrix.ruleArray).toEqualData([ 0, 0, 0, 0, 0, 0, 0, 0 ]);
            expect(ruleMatrix.width).toEqual(51);
            expect(ruleMatrix.value).toEqualData([]);
        });

        it('should add the first row', function () {
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(1);
            expect(ruleMatrix.value[0]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });

        it('should add the second row', function () {
            ruleMatrix.addRow();
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(2);
            expect(ruleMatrix.value[1]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });
    });

    describe('Rule 1', function () {
        var ruleMatrix;

        beforeEach(function () {
            ruleMatrix = new RuleMatrix(1);
        });

        it('should create the correct RuleMatrix for 1', function () {
            expect(ruleMatrix.ruleArray).toEqualData([ 1, 0, 0, 0, 0, 0, 0, 0 ]);
            expect(ruleMatrix.width).toEqual(51);
            expect(ruleMatrix.value).toEqualData([]);
        });

        it('should add the first row', function () {
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(1);
            expect(ruleMatrix.value[0]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });

        it('should add the second row', function () {
            ruleMatrix.addRow();
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(2);
            expect(ruleMatrix.value[1]).toEqualData(
                [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]);
        });
    });

    describe('Rule 30', function () {
        var ruleMatrix;

        beforeEach(function () {
            ruleMatrix = new RuleMatrix(30);
        });

        it('should create the correct RuleMatrix for 30', function () {
            expect(ruleMatrix.ruleArray).toEqualData([ 0, 1, 1, 1, 1, 0, 0, 0 ]);
            expect(ruleMatrix.width).toEqual(51);
            expect(ruleMatrix.value).toEqualData([]);
        });

        it('should add the first row', function () {
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(1);
            expect(ruleMatrix.value[0]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });

        it('should add the second row', function () {
            ruleMatrix.addRow();
            ruleMatrix.addRow();
            expect(ruleMatrix.value.length).toEqual(2);
            expect(ruleMatrix.value[1]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
                  1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });

        it('should add 10 rows', function () {
            var i;
            for (i = 0; i < 10; i++) {
                ruleMatrix.addRow();
            }
            expect(ruleMatrix.value.length).toEqual(10);
            expect(ruleMatrix.value[9]).toEqualData(
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1,
                  0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
        });
    });
});
