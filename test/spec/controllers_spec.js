describe('Controller: RuleVisualizerController', function () {
    'use strict';

    beforeEach(module('RuleVisualizer'));

    var RuleVisualizerController,
        scope;

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        RuleVisualizerController = $controller('RuleVisualizerController', {
            $scope: scope
        });
    }));

    it('true to be truthy', function () {
        expect(true).toBeTruthy();
    });
});
