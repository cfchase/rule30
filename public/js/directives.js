'use strict';

angular.module('RuleVisualizer').directive('binaryGrid', ['_', 'd3',
    function (_, d3) {
        return {
            restrict: 'E',
            template: '<div class="d3svg"></div>',
            scope: {
                grid: '='
            },
            link: function (scope, elem) {
                var svg,
                    height,
                    width,
                    currentRow;

                svg = d3.select(elem[0])
                    .append('svg')
                    .attr('class', 'binary-grid-svg')
                    .attr('preserveAspectRatio', 'xMinYMin');

                scope.$watch('grid.length', drawNewRows);

                scope.$watch('grid[0].length', redraw);

                function redraw() {
                    svg.selectAll('rect').remove();
                    if (!angular.isArray(scope.grid) || !angular.isArray(scope.grid[0])) {
                        return;
                    }

                    height = Math.floor(scope.grid[0].length / 2) * 10;
                    width = scope.grid[0].length * 10;

                    svg.attr('viewBox', '0 0 ' + width + ' ' + height);

                    _.forEach(scope.grid, function (row, index) {
                        drawRow(row, index);
                    });
                }

                function drawNewRows() {
                    var i,
                        start = currentRow + 1,
                        newLength = scope.grid.length - 1;
                    for (i = start; i < newLength; i++) {
                        drawRow(scope.grid[i], i);
                    }
                }

                function drawRow(row, rowIndex) {
                    currentRow = rowIndex;
                    _.forEach(row, function(cell, cellIndex) {
                        if(cell) {
                            drawCellOne(rowIndex, cellIndex);
                        }
                    });
                }

                function drawCellOne(rowIndex, cellIndex) {
                    svg.append('rect')
                        .attr('x', cellIndex * 10)
                        .attr('y', rowIndex * 10)
                        .attr('width', 10)
                        .attr('height', 10)
                        .attr('class', 'cell-one');
                }
            }
        };
    }]);