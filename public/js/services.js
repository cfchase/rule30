'use strict';

angular.module('RuleVisualizer').factory('_', ['$window',
    function ($window) {
        return $window._;
    }]);

angular.module('RuleVisualizer').factory('d3', function ($window) {
    return $window.d3;
});

angular.module('RuleVisualizer').factory('RuleMatrix', ['_',
    function (_) {
        function RuleMatrix(ruleNum) {
            var binStrings = ruleNum.toString(2).split('').reverse();

            this.ruleArray = _.times(8, function (n) {
                return parseInt(binStrings[n], 2) === 1 ? 1 : 0;
            });
            this.width = 51;
            this.value = [];
        }

        RuleMatrix.prototype.increaseSize = function() {
            var numAddedToOneSide = this.width; //will triple size

            this.value = _.map(this.value, function(row) {
                var i,
                    newRow = _.times(numAddedToOneSide, function() {return 0;});

                _.each(row, function(cell) {
                    newRow.push(cell);
                });

                for (i = 0; i < numAddedToOneSide; i++) {
                    newRow.push(0);
                }

                return newRow;
            });

            this.width = this.value[0].length;
        };

        RuleMatrix.prototype.calculateRow = function (rowNum) {
            var self = this,
                newRow,
                previousRow = self.value[rowNum - 1];

            if (!previousRow) {
                newRow = _.times(self.width, function () { return 0; });
                newRow[Math.floor(this.width / 2)] = 1;
            } else {
                newRow = _.times(self.width, function(n) {
                    return self.ruleArray[(previousRow[n-1] || 0) * 4 + (previousRow[n] || 0) * 2 + (previousRow[n+1] || 0)];
                });
            }

            return newRow;
        };

        RuleMatrix.prototype.addRow = function () {
            if (this.width < this.value.length * 2) {
                this.increaseSize();
            }
            this.value.push(this.calculateRow(this.value.length));
        };

        return RuleMatrix;
    }]);


