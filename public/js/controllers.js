'use strict';

angular.module('RuleVisualizer').
    controller('RuleVisualizerController', ['$scope', '$interval', 'RuleMatrix',
        function ($scope, $interval, RuleMatrix) {
            $scope.onStart = function() {
                if (angular.isDefined($scope.running)) {
                    return;
                }

                $scope.running = $interval(function() {
                    $scope.rule30.addRow();
                }, 500);
            };

            $scope.onStop = function() {
                if (angular.isDefined($scope.running)) {
                    $interval.cancel($scope.running);
                    $scope.running = undefined;
                }
            };

            $scope.onReset = function () {
                resetMatrix();
            };

            resetMatrix();

            function resetMatrix() {
                $scope.rule30 = new RuleMatrix(30);
            }
        }]);
