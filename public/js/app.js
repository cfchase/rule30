'use strict';

angular.module('RuleVisualizer', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngRoute'])
    .config(function ($locationProvider) {
        $locationProvider.html5Mode(true);
    });
